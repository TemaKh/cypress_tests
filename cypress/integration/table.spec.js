/// <reference types="cypress" />

describe('MyTestSuite', () => {
    it('Table Test', function () {

        cy.visit('http://testautomationpractice.blogspot.com/')
        cy.title().should('eq', 'Automation Testing Practice')

        // 1) Check Value presence anywhere in table
        cy.get('table[name=BookTable]').contains('td', 'Learn Selenium').should('be.visible')

        // 2) Check Value presence in specific row & column
        cy.get('table[name=BookTable] > tbody > tr:nth-child(2) > td:nth-child(3)').contains('Selenium').should('be.visible')

        // 3) Check Value presence based on condition by iterating rows
        // check the book name "Master In Java" whose author is Amod
        cy.get('table[name=BookTable]>tbody>tr td:nth-child(2)').each(($e, index) => {
            const text=$e.text()
            if(text.includes("Amod")){
                cy.get('table[name=BookTable]>tbody>tr td:nth-child(1)').eq(index).then(function (bname){
                    const bookName = bname.text()
                    expect(bookName).to.equal("Master In Java")
                })
            }
        })

    })
})