/// <reference types="cypress" />

describe('MyTestSuite', () => {

    before(function () {
        cy.fixture('example').then(function (data) {
            this.data = data
        })
    })

    it('FixturesDemoTest', function () {

        cy.visit('https://admin-demo.nopcommerce.com/login')

        cy.get('input[name="Email"]').clear().type(this.data.email)

        cy.get('input[name="Password"]').clear().type(this.data.password)

        cy.get('button[type="submit"]').click()

    })
})