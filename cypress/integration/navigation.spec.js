/// <reference types="cypress" />

describe('MyTestSuite', () => {
    it('Navigation Tests', function () {

        cy.visit('https://demo.nopcommerce.com/')

        cy.title().should('eq', 'nopCommerce demo store') // Home page

        cy.get('.ico-register').contains('Reg').click()

        cy.title().should('eq', 'nopCommerce demo store. Register') // Reg page

        cy.go('back')
        cy.title().should('eq', 'nopCommerce demo store') // Home page

        cy.go('forward')
        cy.title().should('eq', 'nopCommerce demo store. Register') // Reg page

        cy.go(-1) // back
        cy.title().should('eq', 'nopCommerce demo store') // Home page

        cy.go(1) //forward
        cy.title().should('eq', 'nopCommerce demo store. Register') // Reg page

        cy.reload()

    })
})