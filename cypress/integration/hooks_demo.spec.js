/// <reference types="cypress" />

describe('MyTestSuite', () => {

    before(() => {
        // runs once before all tests in the block
        cy.log('************* This is SETUP block *************')
    })

    after(() => {
        // runs once after all tests in the block
        cy.log('************* This is TEAR DOWN block *************')
    })

    beforeEach(() => {
        // runs before each test in the block
        cy.log('************* This is LOGIN block *************')
    })

    afterEach(() => {
        // runs after each test in the block
        cy.log('************* This is LOGOUT block *************')
    })

    it('Searching', function () {
        cy.log('************* This is Searching Test *************')
    })

    it('Advanced searching', function () {
        cy.log('************* This is Advanced Searching Test *************')
    })

    it('Listing Products', function () {
        cy.log('************* This is Listing Products Test *************')
    })
})