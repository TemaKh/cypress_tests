/// <reference types="cypress" />

describe("UI Elements", () => {

    it('Verify Inputbox & Radio buttons', function () {

        cy.visit("https://demo.guru99.com/test/newtours/") // Visit URL

        cy.url().should('include', 'newtours') // Verify URL should include 'newtours'

        cy.get('input[name=userName]').should('be.visible').should('be.enabled').type("mercury")
        cy.get('input[name=password]').should('be.visible').should('be.enabled').type("mercury")

        cy.get('input[name=submit]').should('be.visible').click() // Submit button

        cy.get('td').should('contain.text', 'Login Successfully') // Login verification

        cy.get('[href="reservation.php"]').click()

        cy.title().should('eq', 'Find a Flight: Mercury Tours:') // Title verification

        //Radio Buttons
        cy.get('input[value="roundtrip"]').should('be.visible').should('be.checked') // visibility checked status
        cy.get('input[value="oneway"]').should('be.visible').should('not.be.checked').click()

        cy.get('[name="findFlights"]').should('be.visible').click() // Continue button

        // Check title
    })
})