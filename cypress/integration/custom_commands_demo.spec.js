/// <reference types="cypress" />

describe('CustomSuite', () => {

    it('LoginTest', function () {

        cy.login('admin@yourstore.com', 'admin') // valid
        cy.title().should('be.equal', 'Dashboard / nopCommerce administration')

        cy.login('admin@yourstore.com', 'admin12') // in valid
        cy.title().should('be.equal', 'Your store. Login')

        cy.login('admin@yourstore123.com', 'admin') // in valid
        cy.title().should('be.equal', 'Your store. Login')
    })

    it('Add customer', function () {

        // Login script
        cy.login('admin@yourstore.com', 'admin')

        // Script for Adding new customer
        cy.log('Adding customer..............')

    })

    it('Edit customer', function () {

        // Login script
        cy.login('admin@yourstore.com', 'admin')

        // Script for Editing new customer
        cy.log('Editing customer..............')

    })
})